﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Bouyei.NetFactoryCore.Protocols.WebSocketProt
{
    public class ServerAccessInfo : BaseSendInfo
    {
        /// <summary>
        /// 服务端来源
        /// </summary>
        public string Server { get; set; }
        /// <summary>
        /// 响应日期
        /// </summary>
        public DateTime Date { get; set; }
        /// <summary>
        /// 接入是否验证
        /// </summary>
        public bool AccessCredentials { get; set; }
        /// <summary>
        /// 接入头类型
        /// </summary>
        public string AccessHeader { get; set; }
        /// <summary>
        /// 接入访问验证码
        /// </summary>
        public string SecAccept { get; set; }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(HttpProt))
                HttpProt = "Get /HTTP/1.1";

            if (Date == DateTime.MinValue)
                Date = DateTime.Now;

            if (string.IsNullOrEmpty(Upgrade))
                Upgrade = "WebSocket";

            return string.Format("{0}{1}{2}{3}{4}{5}{6}{7}",
                HttpProt + Environment.NewLine,
                "Connection:" + Connection + Environment.NewLine,
                "Server:" + Server + Environment.NewLine,
                "Upgrade:" + Upgrade + Environment.NewLine,
                 "Date:" + Date.ToString("r") + Environment.NewLine,
                 "Access-Control-Allow-Credentials:" + AccessCredentials + Environment.NewLine,
                 "Access-Control-Allow-Headers:" + AccessHeader + Environment.NewLine,
                 "Sec-WebSocket-Accept:" + SecAccept + Environment.NewLine);
        }
    }
}
