﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Net;

namespace Bouyei.NetFactoryCore.Protocols.WebSocketProt
{
    public class ClientAccessInfo : BaseSendInfo
    {
        /// <summary>
        /// 连接主机
        /// </summary>
        public string Host { get; set; }
        /// <summary>
        /// 连接源
        /// </summary>
        public string Orgin { get; set; }
        /// <summary>
        /// 安全扩展
        /// </summary>
        public string SecExtensions { get; set; }
        /// <summary>
        /// 安全密钥
        /// </summary>
        public string SecKey { get; set; }
        /// <summary>
        /// 安全版本
        /// </summary>
        public string SecVersion { get; set; }

        public override string ToString()
        {
            if (string.IsNullOrEmpty(HttpProt))
                HttpProt = "Get /HTTP/1.1";

            if (string.IsNullOrEmpty(Upgrade))
                Upgrade = "WebSocket";

            return string.Format("{0}{1}{2}{3}{4}{5}{6}{7}",
                HttpProt + Environment.NewLine,
                "Connection:" + Connection + Environment.NewLine,
                "Host:" + Host.ToString() + Environment.NewLine,
                "Origin:" + Orgin + Environment.NewLine,
                "Sec-WebSocket-Extensions:" + SecExtensions + Environment.NewLine,
                "Sec-WebSocket-Key:" + SecKey + Environment.NewLine,
                "Sec-WebSocket-Version:" + SecVersion + Environment.NewLine,
                "Upgrade:" + Upgrade + Environment.NewLine);
        }
    }

    public class BaseSendInfo
    {
        /// <summary>
        /// http连接协议
        /// </summary>
        public string HttpProt { get; set; }
        /// <summary>
        /// 连接类型
        /// </summary>
        public string Connection { get; set; }

        public string Upgrade { get; set; }
    }
}